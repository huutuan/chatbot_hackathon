## ask_sale happy path 1
* user_show_up
    - utter_greet
* greet
    - utter_self_intro
* ask_sale{"product": "Pepsi"}
    - action_response_sale
* thank_you
    - utter_noworries

## ask_sale happy path 2
* user_show_up
    - utter_greet
* greet
    - utter_self_intro
* ask_sale
    - action_response_sale
* thank_you
    - utter_noworries

## ask_sale happy path 3
* user_show_up
    - utter_greet
* greet
    - utter_self_intro
* ask_bot_function
    - utter_show_functions
* ask_sale
    - action_response_sale
* thank_you
    - utter_ask_feedback_message
* user_feedback
    - utter_thanks_for_feedback
* bye
    - utter_bye

## ask_sale happy path 4
* user_show_up
    - utter_greet
* greet
    - utter_self_intro
* ask_bot_function
    - utter_show_functions
* ask_sale{"product": "Pepsi"}
    - action_response_sale
* thank_you
    - utter_ask_feedback_message
* user_feedback
    - utter_thanks_for_feedback
* bye
    - utter_bye

## ask_sale happy path 5
* user_show_up
    - utter_greet
* greet
    - utter_self_intro
* ask_bot_function
    - utter_show_functions
* ask_sale{"place": "VinMall"}
    - action_response_sale
* thank_you
    - utter_ask_feedback_message
* user_feedback
    - utter_thanks_for_feedback
* bye
    - utter_bye
