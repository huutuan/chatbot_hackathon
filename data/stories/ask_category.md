## ask category happy path 1
* user_show_up
  - utter_greet
* greet
  - utter_self_intro
* ask_bot_function
  - utter_show_functions
* ask_category{"category": "nước ngọt"}
  - action_response_category_info
* ask_how_to_paid
  - utter_pay_instruction
* ask_how_to_get_product
  - utter_way_to_get_product
* inform_paid_way{"receive_way": "trực tiếp"}
  - action_get_product_instruction
* thank_you
  - utter_noworries
  - utter_what_help_more

## ask category happy path 2
* user_show_up
  - utter_greet
* ask_bot_function
  - utter_show_functions
* ask_category{"category": "bia"}
  - action_response_category_info
* ask_how_to_paid
  - utter_pay_instruction
* ask_how_to_get_product
  - utter_way_to_get_product
* inform_paid_way{"receive_way": "tại nhà"}
  - action_get_product_instruction
* thank_you
  - utter_noworries
  - utter_what_help_more
