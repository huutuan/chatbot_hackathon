import json
import glob, os, io

PATH_TO_TRAIN_JSON_FILE = 'train_test_split/train/'
PATH_TO_OUTPUT_FILE = 'data/nlu.json'

all_file_json = []

for file in glob.glob(PATH_TO_TRAIN_JSON_FILE + "*.json"):
    all_file_json.append(file)

if len(all_file_json) > 1:
    with io.open(all_file_json[0], 'r', encoding='utf8') as f:
        base_file = json.load(f)

    # print( base_file["rasa_nlu_data"]["common_examples"][8])

    for i in range(1, len(all_file_json)):
        with open(all_file_json[i], 'r', encoding='utf8') as fx:
            update_file = json.load(fx)
        for example in update_file["rasa_nlu_data"]["common_examples"]:
            base_file["rasa_nlu_data"]["common_examples"].append(example)

with io.open(PATH_TO_OUTPUT_FILE, "w", encoding='utf8') as fout:
    # base_file = json.dumps(base_file).encode('utf8')
    # print(base_file)
    json.dump(base_file, fout, ensure_ascii=False, indent=4)
