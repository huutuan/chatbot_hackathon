from rasa.core.channels.slack import SlackInput
from rasa.core.agent import Agent
from rasa.core.interpreter import RasaNLUInterpreter
from rasa.utils.endpoints import EndpointConfig

# Replace <model_directory> with your models directory
nlu_interpreter = RasaNLUInterpreter('./models/20190706-160723/nlu')
# Load agent with created models and connect to action server endpoint, replace <action_server_endpoint> with your endpoint
agent = Agent.load('./models/20190706-160723', interpreter = nlu_interpreter, 
action_endpoint = EndpointConfig('http://localhost:5055/webhook'))

input_channel = SlackInput(
    # this is the `bot_user_o_auth_access_token`
    slack_token="xoxb-686474106336-688325781207-W11bcg5ZxqYJEcAQGCchokuj",
    # slack_channel="@private-channel"
    # the name of your channel to which the bot posts (optional)
    )
s = agent.handle_channels([input_channel], 5005)