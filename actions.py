# -*- coding: utf-8 -*-
from typing import Dict, Text, Any, List, Union, Optional

from rasa_sdk import Tracker, Action
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction
from rasa_sdk.events import SlotSet
# from rasa.core.events import Restarted
from rasa_sdk.events import Restarted
from api_weather import get_weather_info


class UserInforForm(FormAction):
    """
    Handle user information
    """
    def name(self) -> Text:
        """Unique identifier of the form"""

        return "user_info_form"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""
        return ["user_name",]#, "user_relationship"]
        # return []

    def submit(self,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict]:
        # get user_name
        user_name = tracker.get_slot('user_name')
        print(getattr(tracker,"slots"))  #test getattribute func
        print("-----===========-----",user_name)
        dispatcher.utter_template("utter_greet_name",
                                  tracker,
                                  user_name=user_name)
        return []
        # return [SlotSet("user_name", user_name)]

    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
        """A dictionary to map required slots to
            - an extracted entity
            - intent: value pairs
            - a whole message
            or a list of them, where a first match will be picked"""
        return {
            "user_name": self.from_entity(entity="user_name", intent=["introduce"]),
            "user_relationship": [
            self.from_entity(entity="user_relationship"),
            self.from_entity(intent="affirm_agree", value=True),
            self.from_entity(intent="affirm_disagree", value=False),
            ]
        }

    # def submit(
    #     self,
    #     dispatcher: CollectingDispatcher,
    #     tracker: Tracker,
    #     domain: Dict[Text, Any],
    # ) -> List[Dict]:
    #     """Define what the form has to do
    #         after all required slots are filled"""
    #     # utter submit template
    #     user_name = tracker.get_slot('user_name')
    #     if tracker.get_slot('user_relationship'):
    #         user_relationship = "yêu"
    #     else:
    #         user_relationship = "độc thân"
    #     dispatcher.utter_template("utter_show_user_info", tracker, user_name=user_name, user_relationship=user_relationship)
    #     return []

class ActionRestarted(Action):
    """ This is for restarting the chat"""

    def name(self):
        return "action_chat_restart"

    def run(self, dispatcher, tracker, domain):
        return [Restarted()]

class ActionResponseWeather(Action):
    """ This action is for weather information"""

    def name(self):
        return "action_response_weather"

    def run(self, dispatcher, tracker, domain):
        city = tracker.get_slot("capital")
        dispatcher.utter_message("here's the weather in {}:".format(city))
        weather_result = get_weather_info(city)
        dispatcher.utter_message("Search completed:")
        dispatcher.utter_message(str(weather_result))

        return []

class ActionResponseMap(Action):
    """ This action is for map information"""

    def name(self):
        return "action_response_map"

    def map_db(self):
        return {
            "Hot BBQ": {"routes": "Đi thẳng 100m, rẽ trái, lên ngay cầu thang đầu tiên",
                        "image_url": "http://luckyone.com.pk/wp-content/uploads/2017/11/A4_F000.jpg"
                        },
            "VinMall": {"routes": "Đi thẳng 200m, lên cầu thang",
                        "image_url": "https://luckyone.com.pk/wp-content/uploads/2019/06/A4_LGUPDATED01.jpg"
                        }
        }

    def run(self, dispatcher, tracker, domain):
        place = tracker.get_slot("place")
        if place in self.map_db():
            place_map = self.map_db()[place]
            dispatcher.utter_message("{}".format(place_map["routes"]))
            dispatcher.utter_message(place_map["image_url"])
            dispatcher.utter_attachment("/home/tuna/Pictures/abstract-art-background-1037992.jpg")
        else:
            dispatcher.utter_message("Vibo không tìm thấy địa điểm này trong dữ liệu, bạn hỏi lại đi")

        return []

class ActionResponseSale(Action):
    """ This action is for sale information"""

    def name(self):
        return "action_response_sale"

    def sale_db(self):
        return {
            "Coca": "Cocacola ưu đãi 50% khi mua 2 lon Pepsi",
            "Pepsi": "Pepsi vị sữa Giảm 100% khi bạn mua 1 lon Coca special"
        }

    def run(self, dispatcher, tracker, domain):
        obj = tracker.get_slot("object")
        if obj in self.sale_db():
            dispatcher.utter_message("Sale của sản phẩm: {}".format(self.sale_db()[obj]))
        else:
            dispatcher.utter_message("Hiện tại, siêu thị Vivi có những khuyến mại rất hấp dẫn")

        return []

class ActionResponseCategoryInfo(Action):
    """ This action is for category information"""

    def name(self):
        return "action_response_category_info"

    def category_db(self):
        return {
            "nước ngọt": "Nước ngọt sẽ giúp bạn đỡ khát và có nhiều calo",
            "rượu": "Rượu chứa alcohol giúp bạn thăng hoa mà k cần chơi ma túy"
        }

    def run(self, dispatcher, tracker, domain):
        category = tracker.get_slot("category")
        if category in self.category_db():
            dispatcher.utter_message(self.category_db()[category])
            dispatcher.utter_message("Bạn hãy chọn loại {} mà bạn thích trên màn hình".format(category))
        else:
            dispatcher.utter_message("Vibo không tìm thấy thông tin của sản phẩm này trong dữ liệu, xin lỗi về sự bất tiện")

        return []

class ActionGetProductInstruction(Action):
    """ This action is for getting product instruction"""

    def name(self):
        return "action_get_product_instruction"

    def product_info_db(self):
        return {
            "nước ngọt": {"location": "siêu thị Vivi",
                          "map": "link_map",
                          "instruction": "Bạn đi thẳng tới ngã rẽ thứ 2, lên cầu thang, rẽ phải là tới"},
            "rượu": {"location": "quán rượu bà Vivi",
                     "map": "link_map",
                     "instruction": "Bạn đi thẳng tới ngã rẽ thứ 2, lên cầu thang, rẽ phải là tới"},
        }

    def run(self, dispatcher, tracker, domain):
        category = tracker.get_slot("category")
        if category in self.product_info_db():
            dispatcher.utter_message("Bạn hãy đến {}".format(self.product_info_db()[category]["location"]))
            dispatcher.utter_message("Tôi đã mở sẵn map bên phải cho bạn rồi")
            dispatcher.utter_message(self.product_info_db()[category]["instruction"])
        else:
            dispatcher.utter_message("Vibo không tìm thấy thông tin của sản phẩm này trong dữ liệu, xin lỗi vì sự bất tiện")

        return []

class ActionResponseTime(Action):
    """ This action is for time information"""

    def name(self):
        return "action_response_time"

    def run(self, dispatcher, tracker, domain):
        import datetime
        dispatcher.utter_message("Bây giờ là: {}".format(datetime.datetime.now()))
        return []

class ActionResponseObject(Action):
    """ This action is for map information"""

    def name(self):
        return "action_response_object"

    def obj_db(self):
        return ["Pepsi", "Coca", "Áo thun"]

    def run(self, dispatcher, tracker, domain):
        obj = tracker.get_slot("object")
        if obj in self.obj_db():
            dispatcher.utter_message("Tìm thấy sản phẩm: {}".format(obj))
        else:
            dispatcher.utter_message("Vibo không tìm thấy thông tin của sản phẩm này trong dữ liệu, bạn hỏi lại đi")

        return []
