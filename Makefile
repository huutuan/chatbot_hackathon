TRAINTEST_FOLDER = train_test_split/
NLU_CHATETTE_FOLDER = data/nlu_chatette/
NLU_TRAINING_FILE = nlu.json
NLU_DATA_FOLDER = data/

generate-training-data:
	python -m chatette --seed vibo -f -o $(TRAINTEST_FOLDER) --adapter rasa $(NLU_CHATETTE_FOLDER)master.chatette
	python merge_nlu_json.py
